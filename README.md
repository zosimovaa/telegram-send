# Описание

Класс для отправки сообщений в телеграм.
Написан в основном для использования в качестве кастомного StreamHandler для logging (чтобы знать о событиях на vps) 

[Пример кастомного StreamHandler](https://bitbucket.org/zosimovaa/telegram-send/src/master/telegram_send/examples/logging_example.py)