import logging
from logging import StreamHandler
from telegram_send.telegram_send import TelegramSend
import time

class TelegramHandler(StreamHandler, TelegramSend):
    def __init__(self, api_key, chat_id):
        StreamHandler.__init__(self)
        TelegramSend.__init__(self, api_key, chat_id)

    def emit(self, record):
        msg = self.format(record)
        self.send(msg)


if __name__ == "__main__":
    BOT_TOKEN = "344617953:AAECfdvjZEZjSQk4GAfBAT0BlMElXUs2Tn4"
    CHAT_ID = 211945135

    logging.basicConfig()
    logger = logging.getLogger("test logger")
    logger.setLevel(logging.DEBUG)

    consoleHandler = StreamHandler()
    consoleHandler.setLevel(logging.INFO)
    logger.addHandler(consoleHandler)

    telegramHandler = TelegramHandler(BOT_TOKEN, CHAT_ID)
    telegramHandler.setLevel(logging.WARNING)
    logger.addHandler(telegramHandler)

    logger.warning("1 warning message")
    logger.info("2 info message")
    logger.debug("3 debug message")

    telegramHandler.shutdown()
