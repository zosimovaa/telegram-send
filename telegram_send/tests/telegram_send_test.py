import unittest
import time
from telegram_send.telegram_send import TelegramSend

BOT_TOKEN = "344617953:AAECfdvjZEZjSQk4GAfBAT0BlMElXUs2Tn4"
CHAT_ID = 211945135

class MyTestCase(unittest.TestCase):
    def test_send(self):
        t = TelegramSend(BOT_TOKEN, CHAT_ID)
        t.send("test_message")
        data = t.q.get()
        self.assertEqual(data, ('test_message', 211945135))

    def test_spam_control(self):
        #Setup
        t = TelegramSend(BOT_TOKEN, CHAT_ID)
        t.SPAM_COUNT_STOP = 3
        t.SPAM_CONTROL_TIMEOUT = 5
        t.SPAM_BLOCK_TIMEOUT = 2

        #Case 1
        t.spam_control()
        t.spam_control()
        t.spam_control()
        self.assertEqual(t.ready, True)

        #Case 2
        t.spam_control()
        self.assertEqual(t.ready, False)

        #Case 3
        time.sleep(2.1)
        t.spam_control()
        self.assertEqual(t.ready, True)

        #Case 4
        t.spam_control()
        t.spam_control()
        time.sleep(3.1)
        t.spam_control()

        self.assertEqual(t.ready, True)

if __name__ == '__main__':
    unittest.main()
