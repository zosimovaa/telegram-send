import requests
import time
from threading import Thread, Lock
import queue
import logging

logger = logging.getLogger("TelegramSend")

class TelegramSend():
    URL = "https://api.telegram.org/bot{0}/sendMessage?chat_id={1}&text={2}"
    SPAM_COUNT_STOP = 5
    SPAM_CONTROL_TIMEOUT = 60
    SPAM_BLOCK_TIMEOUT = 900

    def __init__(self, api_key, chat_id):
        self.api_key = api_key
        self.chat_id = chat_id

        self.run = True
        self.messages = []
        self.q = queue.Queue(10)

        self.ready = True

        self.spamCount = 0
        self.spamCountBlocked = 0
        self.spamTime = time.time()

        self.worker_thread = Thread(target=self.worker, daemon=True)
        self.worker_thread.start()

    def worker(self):
        """Поток, отвечающий за отправку сообщений из очереди"""
        while self.run:
            if self.q.qsize() > 0:

                message, chat_id = self.q.get()
                self.spam_control()
                if self.ready:
                    self.__send(message, chat_id)
                    self.q.task_done()
            time.sleep(1)

    def shutdown(self):
        self.q.join()
        self.run = False

    def send(self, message):
        result = {}
        try:
            self.q.put((message, self.chat_id), timeout=0)
            result["success"] = True
        except Exception as e:
            result["success"] = False
            result["error"] = str(e)
            logger.error(str(e))
        finally:
            return result


    def spam_control(self):
        self.spamCount += 1
        if self.ready:
            if (self.spamCount > self.SPAM_COUNT_STOP) and ((time.time() - self.spamTime) < self.SPAM_CONTROL_TIMEOUT):
                self.ready, self.spamCount = False, 0
                logger.info("Spam detected")
        else:
            if time.time() - self.spamTime > self.SPAM_BLOCK_TIMEOUT:
                self.ready, self.spamCount = True, 0
                #self.spamCountBlocked = self.spamCount


    def __send(self, message, chatid):
        try:
            url = self.URL.format(self.api_key, chatid, message)
            resp = requests.get(url).json()
            #todo сделать обработку результата отправки
        except Exception as e:
            logger.error(str(e))
